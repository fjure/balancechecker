const Web3 = require("web3");
const nodemailer = require("nodemailer");
const { config } = require("./config");

const web3 = new Web3(config.nodeHost);

const report = {};

const start = async () => {
  const sendReport = false;
  for (let index = 0; index < config.addressesToCheck.length; index++) {
    const address = config.addressesToCheck[index];
    const balance = await web3.eth.getBalance(address);
    const needsTransfer = balance < config.minBalance;
    if (needsTransfer) sendReport = true;
    report[address] = {
      balance,
      needsTransfer
    };
  }

  if (sendReport) {
    let transporter = nodemailer.createTransport({
      service: "Gmail",
      auth: {
        user: config.mail, // generated ethereal user
        pass: config.password // generated ethereal password
      }
    });

    const info = await transporter.sendMail({
      from: '"COA BALANCES REPORTER"',
      to: config.receivers,
      subject: "ADDRESSES REPORT",
      text: "Report", // plain text body
      html: `${JSON.stringify(report)}` // html body
    });

    console.log("Mail sended: ",info);
  }

  console.log("Report of balances:", report);
};

start();
